array = [1,2,3,3,4,5,5]

#Dodawanie
array.push(6)
array << 7
print array


#unshift doda element na początku tablicy
array.unshift(0)

#insert dodaje w konkretne miejsce
array.insert(3,2.5)

#Usuwanie
#.pop usuwa ostani element
array.pop
#.uniq usuwa duble
array.uniq

array.each {
|a| puts a
}

#.map tworzenie nowej tablicy na podstawie starej
mynew_array = array.map { |i| i**2}

#wybieranie z tablicy konkretnych danych

array.select { |i| i>3}
